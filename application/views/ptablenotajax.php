<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include("mdls/mdl_posts.php");
?>
<script type='text/javascript'>
var base_url = "<?php echo base_url();?>";
</script>
<div id="container" class="container mt-5">
	<div class="row">
		<div class="col-md-12 mt-4">
			<div class="jumbotron">
				<h3>Paginación en tablas sin ajax</h3>
				<p class="lead"></p>
			</div>
		</div>
	</div>

	<form method="get" enctype="multipart/form-data" accept-charset="utf-8" id="form-search" name="form-search" action="<?=base_url('ptablenotajax');?>">
		<div class="form-row mt-2 mb-1">
			<div class="col-md-1 col-sm-1 col-2">
				<select class="form-control" name="limit">
					<option selected="" value="10" <?=($limit&&$limit==10?'selected=""':'');?>>10</option>
					<option value="10" <?=($limit&&$limit==20?'selected=""':'');?>>20</option>
					<option value="20" <?=($limit&&$limit==30?'selected=""':'');?>>30</option>
					<option value="30" <?=($limit&&$limit==40?'selected=""':'');?>>40</option>
				</select>
			</div>
			<div class="col-md-9 col-sm-11 col-10">
				<div class="input-group">
					<div class="input-group-prepend">
						<select class="form-control" name="edo">
							<option value="2" <?=($edo==1?'selected=""':'');?>>Activos</option>
							<option value="1" <?=($edo==0?'selected=""':'');?>>Inactivos</option>
						</select>
					</div>
					<input type="text" class="form-control txt-search-nv" name="search" id="txt-search" placeholder="Buscar..." value="<?=($search?$search:'');?>">
					<div class="input-group-append">
						<button type="submit" class="btn btn-search-nav" id="btn-search">
							<i class="fal fa-search"></i>
						</button>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-2 col-2">
				<button class="btn btn-block btn-success" title="Agregar Sección" data-toggle="modal" data-target="#mdl-add-reg">
					<i class="fa fa-plus-circle"></i> Agregar Registro
				</button>
			</div>
		</div>
	</form>
	<div class="row">
		<div class="col-md-12 text-right">
			<h5 id="h5-cnt-total"><?=($total?'Total de resultados: '.$total:'');?></h5>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php
			echo '<div class="table-responsive">
					<table  class="table table-hover">
						<thead>
							<tr class="row-link">
								<th data-field="id_post" class="th-link text-left"> <i class="fas fa-sort"></i> Id </th>
								<th data-field="nom_post" class="th-link"><i class="fas fa-sort"></i> Nombre</th>
								<th class="w-5 text-center">Acciones</th>
							</tr>
						</thead>
						<tbody>';
			foreach($results as $post){
				echo '<tr id="row-id-'.$post->id_post.'">
						<td class="text-left">
							'.$post->id_post.'
						</td>
						<td>
							'.$post->nom_post.'
						</td>
						<td class="text-center">
							<button type="button" class="btn btn-danger btn-sm mdl-del-reg" title="Eliminar" data-toggle="modal" data-target="#mdl-del-reg" data-idreg="'.$post->id_post.'" data-nomreg="'.$post->nom_post.'">
								<i class="fal fa-trash-alt"></i>
							</button>
						</td>
					</tr>';
			}
			echo '</tbody></table></div>';
			?>
		</div>
	</div>
	<div class="row mt-3">
		<div class="col-md-12 mb-4">
			<div id="pag-temas">
				<?php
					if(isset($links)){
						echo $links;
					} 
				?>
			</div>
		</div>
	</div>
	<!-- /content ajax load pagination -->
</div>

<script src="<?php echo base_url();?>assets/app/ajax/notajxtablepage.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		//load(0);
	});
</script>