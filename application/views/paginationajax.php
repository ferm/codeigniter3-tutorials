<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type='text/javascript'>
var base_url = "<?php echo base_url();?>";
</script>
<div id="container" class="container mt-4">
	<div class="row">
		<div class="col-md-12 mt-5">
			<div class="jumbotron">
				<h3>Paginación completa con ajax</h3>
				<p class="lead"></p>
			</div>
		</div>
	</div>

	<!-- content ajax load pagination -->
	<div id="div-cnt-ajax" class="row"></div>
	<div class="row mt-3">
		<div class="col-md-12 mb-4">
			<div id="pag-temas"></div>
		</div>
	</div>
	<!-- /content ajax load pagination -->
</div>

<script src="<?php echo base_url();?>assets/app/ajax/ajxfuctloadexmp.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		load(0);
	});
</script>