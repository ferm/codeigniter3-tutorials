<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type='text/javascript'>
var base_url = "<?php echo base_url();?>";
</script>
<div id="container" class="container mt-5">
	<div class="row">
		<div class="col-md-12 mt-5">
			<div class="jumbotron">
				<h3>Paginación completa sin ajax</h3>
				<p class="lead"></p>
			</div>
		</div>
	</div>

	<!-- content ajax load pagination -->
	<div id="div-cnt-ajax" class="row">
		<?php
		foreach($results as $tp){
			echo '<div class="col-md-4 col-sm-6 col-xs-6 mb-2">
						<div class="card mb-2">
								<a href="#">
									<img src="'.base_url().'assets/app/images/default_post.png" class="card-img-top">
								</a>
								<div class="card-body font-weight-bold ">
									<a class="text-dark" href="#">
										'.$tp->nom_post.'
									</a>
									<div>
										<small class="float float-right"><span class="byline-author-label">By</span>
											<a class="byline-author-name-link" href="#" title="LiNuXiToS">LiNuXiToS</a></small>
										<small>'.mesDiaAnio($tp->fc_post).'</small>
									</div>
								</div>
							</div>
					</div>';
		}
		?>
	</div>
	<div class="row mt-3">
		<div class="col-md-12 mb-4">
			<div id="pag-temas">
				<?php
					if(isset($links)){
						echo $links;
					} 
				?>
			</div>
		</div>
	</div>
	<!-- /content ajax load pagination -->
</div>